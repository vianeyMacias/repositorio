
$('#botonEnviar').click(function(){
        var regex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
        if(!(regex.test($('#email').val().trim()))){
        alert('Ingresa una dirección de correo eléctronico valida').finish();
}
});

$('#botonEnviar').click(function(){
        if(!($('#facebook').val().trim() != '' || $('#instagram').val().trim() != '' || $('#youtube').val().trim() != '')){
        alert('Ingresa al menos una red social').finish();
}
});

$('#botonEnviar').click(function(){
        if(!($('#name').val().trim() != '' )){
        alert('Ingresa tu nombre completo').finish();
}
});

$('#botonEnviar').click(function(){
        if(!($('#comments').val().trim() != '' )){
        alert('Ingresa un comentario').finish();
}
});


$(document).ready(function(){
  $('#botonEnviar').hide();
     $('.social-network').blur(function(event){
            if($('#facebook').val().trim() != '' || $('#instagram').val().trim() != '' || $('#youtube').val().trim() != ''){
            $('#botonEnviar').show('slow');
            } else {
              $('#botonEnviar').hide('slow');
            }
     });
     $('#terms-and-privacy').attr('checked','checked');
     $('#terms-and-privacy').click(function(){
            if($('#terms-and-privacy').is(':checked')) {
              $('#botonEnviar').show('slow');
            } else {
              $('#botonEnviar').hide('slow');
            }
            return true;
     });

 });


$('#botonEnviar').click(function(event){
event.preventDefault();
  $.ajax(window.endpoint +  '/blogger/index',
  {
    data    : {
      name      : $('#name').val(),
      email     : $('#email').val(),
      facebook  : $('#facebook').val(),
      instagram : $('#instagram').val(),
      youtube   : $('#youtube').val(),
      comments  : $('#comments').val(),
    },
    method      : 'post',
    crossDomain : true,
    dataType    : 'json',
    success     : function(result, status){
/*      if(undefined !== data.ok){
        if(data.ok){*/
          $("#myModal").modal('show');
/*        }else{
          alert('Error al registrarse');
        }
      }*/
    },
    error    : function(data, status){
   //   alert('Error al registrarse');
        $("#myModal").modal('show');
    },
    complete : function(data, status){
      $("#name").val('').focus();
      $("#email").val('');
      $("#facebook").val('');
      $("#instagram").val('');
      $("#youtube").val('');
      $("#comments").val('');
      $('#botonEnviar').hide('slow');
      /*$('#terms-and-privacy').attr('checked', false);*/
    },
  }
  );
}); 
