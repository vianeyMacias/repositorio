<?php

error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 1);

	$config = require '.config.php';
	$url = $config['endpoint'];

?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">


<title>Movimiento Ilusion</title>

<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5TJBSC');</script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-7035812-10"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-7035812-10');
</script>


</head>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/landing.css">
<script src="<?= $config['endpoint'] ?>/pub/static/frontend/Magento/ilusionmx/es_MX/jquery.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/TweenMax.min.js"></script>
<script src="js/ScrollMagic.min.js"></script>
<script src="js/debug.addIndicators.min.js"></script>
<script src="js/animation.gsap.js"></script>
<script src="js/ScrollToPlugin.min.js"></script>
<script src="js/animation.gsap.js"></script>
<body>


<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5TJBSC"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>


  <p id="url_post"> <?php echo($url)?> </p>
    <div class="contenedor">
        <section id="screen-1">
            <img class="desktop-image-screen1" src="img/desktop/screen_1/background-up.png">
            <img class="mobile-image-screen1" src="img/mobile/screen_1/grafico.png">
            <div class="text-content text-center white-letter">
                <div>¡SÉ TU MISMA!</div>
                <div>Y ÚNETE AL <strong>MOVIMIENTO ILUSIÓN</strong></div>
                <div>TÚ ERES QUIÉN NOS INSPIRA CADA DÍA</div>
                <div>AHORA ES TU TURNO. ÚNETE AL MOVIMIENTO ILUSIÓN Y AYÚDANOS A INSPIRAR A MÁS MUJERES COMO TÚ</div>
                <div class="know-you">¡QUEREMOS CONOCERTE!</div>
            </div>
        </section>

        <section id="screen-2">
            <img class="desktop-image-screen2" src="img/desktop/screen_2/background-2.png">
            <img class="mobile-image-screen2" src="img/mobile/screen_2/cubo-formulario.png">
            <img class="circle-image" src="img/desktop/screen_2/circulo.png">
            <div class="row second-screen">
          <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <img class="popup-image" src="img/desktop/screen_2/popup-gracias.png">
        </div>
      </div>
      
    </div>
  </div>
                <div class="col-md-12 col-lg-4">
                    <div class="contact-text white-letter">
                        <div>QUEREMOS</div>
                        <div>CONOCERTE</div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-8">
                    <div id="formulario">
                        <form class="formulario" id="formdata">
                            <div class="form-group row">
                                <label for="nombreText" class="col-sm-3 col-lg-3 col-form-label white-letter">NOMBRE</label>
                                <div class=" col-sm-9 col-lg-8" >
                                    <input type="text" class="form-control input-sm" id="name" name="name" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="emailText" class="col-sm-3 col-lg-3 col-form-label white-letter">MAIL</label>
                                <div class="col-sm-9 col-lg-8">
                                    <input type="email" class="form-control" id="email" name="email" required>
                                    <span class="valide-email white-letter" id="emailOK"></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="facebookText" class="col-sm-3 col-lg-3 col-form-label white-letter">FACEBOOK</label>
                                <div class="col-sm-9 col-lg-8">
                                  <input type="text" class="form-control social-network" id="facebook" name="facebook">
                              </div>
                          </div>
                          <div class="form-group row">
                            <label for="instagramText" class="col-sm-3 col-lg-3 col-form-label white-letter">INSTAGRAM</label>
                            <div class="col-sm-9 col-lg-8">
                              <input type="text" class="form-control social-network" id="instagram" name="instagram" >
                          </div>
                      </div>
                      <div class="form-group row">
                        <label for="youtubeText" class="col-sm-3 col-lg-3 col-form-label white-letter">YOUTUBE</label>
                        <div class="col-sm-9 col-lg-8">
                          <input type="text" class="form-control social-network" id="youtube" name="youtube">
                      </div>
                  </div>
                  <div class="form-group row">
                    <label for="cuentanosText" class="col-sm-12 col-lg-11 col-form-label white-letter">CUÉNTANOS, ¿QUÉ TE INSPIRA?</label>
                    <div class="col-sm-12 col-lg-11">
                      <textarea type="text" rows="4" class="form-control" id="comments" name="comments" required></textarea>
                  </div>
              </div>
              <div class="white-letter terms-and-conditions">Mayor de 18 años
                        <div class="form-check">
                          <label class="form-check-label">
                            <input type="checkbox" class="form-check-input" id="terms-and-privacy" name="terms-and-privacy" required checked>Acepto <a href="https://www.ilusion.com/aviso-privacidad/" target="_blank" class="white-letter">Aviso de privacidad</a> y <a href="https://www.ilusion.com/terminos-condiciones/" target="_blank" class="white-letter">Términos y Condiciones</a>
                          </label> 
                        </div>
                      </div>
              <div><input type="image" class="boton" id="botonEnviar" src="img/mobile/screen_2/boton.png" ></div>
          </form>

      </div>
  </div>
</div>
<img class="roses-image" src="img/desktop/screen_2/rosas.png">
</section>
</div>
<script type="text/javascript">
    var endpoint = '<?= $config['endpoint'] ?>';
</script>
<script src="js/landing.js" type="text/javascript"></script>

</body>
</html>
